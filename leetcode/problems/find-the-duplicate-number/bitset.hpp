// Solution that uses bitset.
// It IS in O(1) space xD, we know max key value
//
// Leetcode stats:
//  Runtime: 169 ms
//  Memory Usage: 61.3 MB
// Well: looking into floyd's tortoise solution...
//       it seems that memory usage is computed somehow weird...

#pragma once

constexpr int KEY_MAX { 100001 };

class Solution {
    uint32_t hits [ ( KEYMAX + 31 ) / 32 ] = { 0 };

  public:
    int findDuplicate( vector<int> &nums ) {
        auto const n { nums.size() - 1 };
        for ( auto v : nums ) {
            auto const i { v / 32 };
            auto const mask { ( 1ull << ( v & 0x1f ) ) };
            if ( hits [ i ] & mask ) {
                return v;
            }
            hits [ i ] |= mask;
        }
        assert( false );
        return 0;
    }
};
