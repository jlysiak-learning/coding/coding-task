class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int M{ -100000 }; 
        int s{ 0 };
        for ( auto v : nums ) {
            if ( s + v > M ) {
                M = s + v;
            }
            if ( v + s > 0) {
                s += v;
            } else {
                s = 0;
            }
        }
        return M;
    }
};
