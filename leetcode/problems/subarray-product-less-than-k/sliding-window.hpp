class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        auto count{ int{} };
        auto l{ int {} };
        auto product{ 1 };
        for ( auto r{ int{} }; r < nums.size(); ++r ) {
            product *= nums[ r ];
            // keep invariant, for each r in arr, find min l that prod[l : r] < k
            while ( l <= r && product >= k ) {
                product /= nums[ l ];
                ++l;
            }
            if ( l <= r ) {
                count += r - l + 1;
            }
        }
        return count;
    }
};
