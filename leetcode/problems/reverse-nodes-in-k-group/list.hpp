/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        if ( k == 1 ) {
            return head; // no reversing needed
        }
        
        auto new_head{ static_cast< ListNode * >( nullptr ) };
        auto last{ static_cast< ListNode * >( nullptr ) };
        auto begin{ head };
        auto end{ begin };
        auto sz{ 1 };
        while ( end ) {
            if ( sz == k ) {
                // reverse
                auto [ rh, rt ]{ reverseList( begin, end ) };
                sz = 1;
                begin = rt->next;
                end = begin;
                if ( last ) {
                    last->next = rh;
                } else {
                    new_head = rh;
                }
                last = rt;
            } else {
                ++sz;
                end = end->next;
            }
        }
        return new_head ? new_head : head;
    }
    
private:
    std::pair< ListNode*, ListNode * > reverseList( ListNode * h, ListNode * t ) {
        //  a    ->   b     ->    c -> d  ==> d -> c -> b -> a
        //  ^node -> ^next             ^ node
        assert( h );
        assert( t );
        auto const after_t{ t->next };
        
        auto prev{ h };
        auto curr{ h->next };
        while( curr != after_t ) {
            auto n{ curr->next };
            curr->next = prev;
            prev = curr;
            curr = n;
        } 
        h->next = after_t;
        return std::make_pair( t, h );
    }
};
